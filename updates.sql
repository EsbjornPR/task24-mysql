UPDATE journal
SET event_type = 'Home care patient'
WHERE id =1; 

UPDATE journal
SET event_type = 'Hospital care'
WHERE id =2; 

UPDATE journal
SET event_type = 'Private care facility'
WHERE id =3; 

UPDATE author
SET first_name = 'Rick'
WHERE id =1; 

UPDATE author
SET surname = 'Nolan'
WHERE id =2; 

UPDATE author
SET first_name = 'Lauren', surname = 'Butterscotch'
WHERE id =3; 

UPDATE entry
SET title = 'Routine examination'
WHERE id = 1;

UPDATE entry
SET title = 'Routine examination'
WHERE id = 2;

UPDATE entry
SET description = 'All tests show good results and the patient feels fully recovered. Case closed'
WHERE id = 3;

UPDATE tag
SET name = 'Covid-19 alert'
WHERE id = 1;

UPDATE tag
SET name = 'Measels'
WHERE id = 2;

UPDATE tag
SET name = 'Rubella'
WHERE id = 3;

UPDATE entry_tag
SET tag_id = '3'
WHERE id = 3;