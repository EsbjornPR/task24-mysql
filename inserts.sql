INSERT INTO journal (event_type)
VALUES ('Home patient');

INSERT INTO journal (event_type)
VALUES ('Hospitalized');

INSERT INTO journal (event_type)
VALUES ('Local Care facility');

INSERT INTO author (first_name, surname)
VALUES ('Daniel', 'Snozcumber');

INSERT INTO author (first_name, surname)
VALUES ('Kate', 'Kate');

INSERT INTO author (first_name, surname)
VALUES ('Fairydust', 'Bogtrotter');

INSERT INTO entry (title, description, journal_id, author_id)
VALUES ('Routin examination', 'Slight fever 38C otherwise in good condition. Minor rash. Improving', '1', '2' );

INSERT INTO entry (title, description, journal_id, author_id)
VALUES ('Routin examination', 'High fever 41C. Breathing issues. Corona sampling made. Patient isolated in wait fopr results', '3', '3' );

INSERT INTO entry (title, description, journal_id, author_id)
VALUES ('Post examination', 'All tests show good results and the patient feels fully recovered.', '2', '1' );

INSERT INTO tag (name)
VALUES ('Covid-19');

INSERT INTO tag (name)
VALUES ('measels');

INSERT INTO tag (name)
VALUES ('rubella');

INSERT INTO entry_tag ( entry_id, tag_id)
VALUES ( '1', '3');

INSERT INTO entry_tag ( entry_id, tag_id)
VALUES ( '1', '1');

INSERT INTO entry_tag ( entry_id, tag_id)
VALUES ( '2', '1');

INSERT INTO entry_tag ( entry_id, tag_id)
VALUES ( '3', '1');
