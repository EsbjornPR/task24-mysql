/* 1-Show all journals and their possible entries */
SELECT journal.id, journal.event_type, entry.title, entry.description, entry.created 
FROM journal
LEFT JOIN entry
ON journal.id = entry.journal_id;

/* 2-Show all entries and their writers */
SELECT entry.title, entry.description, author.first_name, author.surname, entry.created
FROM entry
LEFT JOIN author
ON entry.author_id = author.id;

/* 3-Show all entries, that have authors, and all writers whether they produced or not  */
SELECT entry.title, entry.description, author.first_name, author.surname, entry.created
FROM entry
RIGHT JOIN author
ON entry.author_id = author.id;

/* 4-Show journals, their entries and entry writers */
SELECT journal.id, journal.event_type, entry.title, entry.description, author.first_name, author.surname, entry.created
FROM ((journal
INNER JOIN entry ON journal.id = entry.journal_id)
INNER JOIN author ON entry.journal_id = author.id);

/* 5-Show all entries and their tags */
SELECT entry.id, entry.title, entry.description, tag.name, entry.created
FROM ((entry
INNER JOIN entry_tag ON entry.id = entry_tag.entry_id)
INNER JOIN tag ON tag.id = entry_tag.tag_id);

/* 6-Show all entries and group tags to one cell/entry */
SELECT entry.id, entry.title, entry.description, GROUP_CONCAT(tag.name) AS tags, entry.created
FROM entry
INNER JOIN entry_tag ON entry.id = entry_tag.entry_id
INNER JOIN tag ON tag.id = entry_tag.tag_id
GROUP BY entry.id;

/* 7-Show tags and related journals */
SELECT tag.id, tag.name, GROUP_CONCAT(journal.id) AS Effected_journals
FROM tag
INNER JOIN entry_tag ON tag.id = entry_tag.tag_id
INNER JOIN entry ON entry_tag.entry_id = entry.id
INNER JOIN journal ON entry.journal_id = journal.id
GROUP BY tag.id;

/* 8-WHat journals the different author's have contributed to */
SELECT author.first_name, author.surname, GROUP_CONCAT(journal.id) AS Contributed_in_journals
FROM author
LEFT JOIN entry ON entry.author_id = author.id
LEFT JOIN journal ON journal.id = entry.journal_id
GROUP BY author.surname;

/* 9- List all entries with journal belonging, author and tags. */
SELECT entry.id, entry.title, entry.description, author.first_name, author.surname, entry.created, GROUP_CONCAT(tag.name) AS tags
FROM entry
LEFT JOIN journal ON entry.journal_id = journal.id
LEFT JOIN author ON entry.author_id = author.id
LEFT JOIN entry_tag ON entry.id = entry_tag.entry_id
LEFT JOIN tag ON tag.id = entry_tag.tag_id
GROUP BY entry.id;

/* 10- List what tags the different authors use */
SELECT author.first_name, author.surname, GROUP_CONCAT(tag.name) AS Tags_used
FROM author
LEFT JOIN entry ON entry.author_id = author.id
LEFT JOIN entry_tag ON entry.id = entry_tag.entry_id
LEFT JOIN tag ON tag.id = entry_tag.tag_id
GROUP BY author.surname;

