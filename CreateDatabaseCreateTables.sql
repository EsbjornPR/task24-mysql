CREATE DATABASE myjournal;
USE myjournal;

CREATE TABLE journal (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	event_type VARCHAR(128) ,
    created TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE author (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    first_name VARCHAR(80) NOT NULL,
	surname VARCHAR(128) NOT NULL
);
    
CREATE TABLE entry (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	title VARCHAR(128) NOT NULL,
    description TEXT,
    created TIMESTAMP NOT NULL DEFAULT NOW(),
	journal_id INT,
    author_id INT,
	FOREIGN KEY (journal_id) REFERENCES journal(id),
    FOREIGN KEY (author_id) REFERENCES author(id)
);

CREATE TABLE tag (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	name VARCHAR(128) NOT NULL
);

CREATE TABLE entry_tag (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    entry_id INT,
    tag_id INT,
	FOREIGN KEY (entry_id) REFERENCES entry(id),
    FOREIGN KEY (tag_id) REFERENCES tag(id)
);

